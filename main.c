#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include "Config.h"

/**
* PIC Configurations
*/
#pragma config FOSC = XT
#pragma config WDTE = OFF
#pragma config PWRTE = OFF
#pragma config MCLRE = ON
#pragma config CP = OFF
#pragma config CPD = OFF
#pragma config BOREN = OFF
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF

#pragma config BOR4V = BOR40V
#pragma config WRT = OFF

void interrupt isr(void);
void play_tone(float freq, int dc);
void set_timer2(int prsc);
void play_twinkle_star(void);
void unset_pwm(void);
void delay(float milliseconds);

float pwmfreq = 0.0;
float pwmperiod = 0.0;
float oscfreq = 4000000.0;
float oscperiod = 0.0;
int pr2value = 0;
int maxdutycycle = 0;
int dutycycle = 0;
int started = 0;

int main(int argc, char** argv) {
	config_board();

	while(1) {
		if(PORTDbits.RD0 == 1) {
			play_tone(C4, 50);
			LEDC = _ON;
		}
		else if(PORTDbits.RD1 == 1) {
			play_tone(D4, 50);
			LEDD = _ON;
		}
		else if(PORTDbits.RD2 == 1) {
			play_tone(E4, 50);
			LEDE = _ON;
		}
		else if(PORTDbits.RD3 == 1) {
			play_tone(F4, 50);
			LEDF = _ON;
		}
		else if(PORTDbits.RD4 == 1) {
			play_tone(G4, 50);
			LEDG = _ON;
		}
		else if(PORTDbits.RD5 == 1) {
			play_tone(A4, 50);
			LEDA = _ON;
		}
		else if(PORTDbits.RD6 == 1) {
			play_tone(B4, 50);
			LEDB = _ON;
		}
		else {
			PORTB = _OFF;
			PORTEbits.RE0 = _OFF;
			unset_pwm();
		}
	}

	return (EXIT_SUCCESS);
}

void interrupt isr(void){
	if (PIR1bits.TMR2IF == 1 && started == 0) {				// Se activa el evento especial
		started = 1;
		TRISCbits.TRISC2 = _OUTPUT;						// CCP1 => OUTPUT
		BUZZER = _ON;
	}
	PIR1bits.TMR2IF = 0;
}

void delay(float milliseconds) {
	int count = (int)milliseconds;
	for (int i = 0; i < count; i++) {
		__delay_ms(1);
	}
}

void unset_pwm(void) {
	TRISCbits.TRISC2 = _INPUT;

	PIE1bits.TMR2IE = 0;														// Enables the TMR2 interrupt
	PIR1bits.TMR2IF = 0;														// Clear TMR2 Flag
	BUZZER = _OFF;
	started = 0;
}

void play_tone(float freq, int dc) {
	pwmfreq = freq;
	dutycycle = dc;
	oscperiod = (float)(1.0/oscfreq);
	pwmperiod = (float)(1.0/pwmfreq);

	for (int i = 1; i <= 16; i = i*4)
	{
		pr2value = (int)(pwmperiod/(i*oscperiod*4.0)-1.0);
		if(pr2value > 255 || pr2value < 1) {
			continue;
		}
		else {
			maxdutycycle = (int)(pwmperiod/(oscperiod*i));
			dutycycle = (int)(((float)dc/100.0)*((float)maxdutycycle));

			PIE1bits.TMR2IE = 1;														// Enables the TMR2 interrupt
			PIR1bits.TMR2IF = 0;														// Clear TMR2 Flag

			// Configure CCP1
			PR2 = (int)pr2value;
			CCP1CON = ((dutycycle & 3) << 4) + 12;
			CCPR1L = dutycycle >> 2;

			// Configure TMR2
			set_timer2(i);
			break;
		}
	}
}

void set_timer2(int prsc) {
	if(prsc == 1) {
		T2CON = 0b00000100;
	}
	else if(prsc == 4) {
		T2CON = 0b00000101;
	}
	else if(prsc == 16) {
		T2CON = 0b00000111;
	}
}

void play_twinkle_star(void) {
	float song[]={C4, C4, G4, G4, A4, A4, G4, F4, F4, E4, E4, D4, D4, C4};					// Notes of song
	float length[]={E, E, E, E, E, E, Q, E, E, E, E, E, E, Q}; 								// Notes length

	for (int i=0; i<14; i++) {
		play_tone(song[i], 50);
		delay(length[i]);
		unset_pwm();
		__delay_ms(50);
	}
}