#ifndef GLOBALDEFINITIONS_H
#define	GLOBALDEFINITIONS_H

#define true  1
#define false 0

#define _ON  1
#define _OFF 0

#define _INPUT 1
#define _OUTPUT 0

#define BUZZER PORTCbits.RC2
#define LEDC PORTBbits.RB0
#define LEDD PORTBbits.RB1
#define LEDE PORTBbits.RB2
#define LEDF PORTBbits.RB3
#define LEDG PORTBbits.RB4
#define LEDA PORTBbits.RB5
#define LEDB PORTEbits.RE0

//#define BUZZER      PORTAbits.RA0

/* Defining Musical Notes Frequencies (NOTE : X Hz) */
#define	C4	261.63
#define	Db4	277.18
#define	D4	293.66
#define	Eb4	311.13
#define	E4	329.63
#define	F4	349.23
#define	Gb4	369.99
#define	G4	392.00
#define	Ab4	415.30
#define	A4	440.00
#define	Bb4	466.16
#define	B4	493.88
#define	C5	523.25
#define	Db5	554.37
#define	D5	587.33
#define	Eb5	622.25
#define	E5	659.26
#define	F5	698.46
#define	Gb5	739.99
#define	G5	783.99
#define	Ab5	830.61
#define	A5	880.00
#define	Bb5	932.33
#define	B5	987.77
#define	C6	1046.50
#define	Db6	1108.73
#define	D6	1174.66
#define	Eb6	1244.51
#define	E6	1318.51
#define	F6	1396.91
#define	Gb6	1479.98
#define	G6	1567.98
#define	Ab6	1661.22
#define	A6	1760.00
#define	Bb6	1864.66
#define	B6	1975.53
#define	C7	2093.00
#define	Db7	2217.46
#define	D7	2349.32
#define	Eb7	2489.02
#define	E7	2637.02
#define	F7	2793.83
#define	Gb7	2959.96
#define	G7	3135.96
#define	Ab7	3322.44
#define	A7	3520.01
#define	Bb7	3729.31
#define	B7	3951.07
#define	C8	4186.01
#define	Db8	4434.92
#define	D8	4698.64
#define	Eb8	4978.03
#define R   0.0

// DURATION OF THE NOTES
#define BPM     105                                                             // For playing tunes purposes
#define Q       60000/BPM                                                       // Quarter note (1/4)
#define H       2*Q                                                             // Half note (1/2)
#define E       Q/2                                                             // 8th note (1/8)
#define S       Q/4                                                             // 16th note (1/16)
#define W       4*Q                                                             // Whole note 4/4

#endif	/* GLOBALDEFINITIONS_H */